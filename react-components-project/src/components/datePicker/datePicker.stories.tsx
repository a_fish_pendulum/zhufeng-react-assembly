import React from "react";
import { DatePicker } from "./index";

import { withKnobs } from "@storybook/addon-knobs";
export default {
  title: "datePicker",
  component: DatePicker,
  decorators: [withKnobs],
};

export const Demo = () => {
  return <DatePicker />;
};
