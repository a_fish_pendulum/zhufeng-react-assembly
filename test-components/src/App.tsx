import React from "react";
import {Button} from "yh-react";
import { GlobalStyle } from "yh-react";

function App() {
	return (
		<div className="App">
			<GlobalStyle></GlobalStyle>
			<Button appearance="primary">2222</Button>
		</div>
	);
}

export default App;